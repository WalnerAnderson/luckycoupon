<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Cadastro | Lucky Coupon</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="_luckycoupon_sigilo_/img/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="_luckycoupon_sigilo_/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="_luckycoupon_sigilo_/font/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="_luckycoupon_sigilo_/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="_luckycoupon_sigilo_/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="_luckycoupon_sigilo_/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="_luckycoupon_sigilo_/css/util.css">
	<link rel="stylesheet" type="text/css" href="_luckycoupon_sigilo_/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="_luckycoupon_sigilo_/img/img-01.png" alt="IMG">
				</div>

				<form class="login100-form validate-form" action="config/config_usuario" method="POST">
					<span class="login100-form-title">
						Cadastre-se agora!
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="email" placeholder="Email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit" name="insert">
							Cadastrar
						</button>
					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
							Possui Cadastro?
						</span>
						<a class="txt2" href="login">
							Login
						</a>
					</div><br/><br/><br/><br/>
				</form>
				<p class="messagem">
                    <?php
                        if(isset($_SESSION['error'])){ 
                            echo $_SESSION['error']; 
                            unset($_SESSION['error']);
                        }
                    ?>    
                </p>
			</div>
		</div>
	</div>
<!--===============================================================================================-->	
	<script src="_luckycoupon_sigilo_/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="_luckycoupon_sigilo_/vendor/bootstrap/js/popper.js"></script>
	<script src="_luckycoupon_sigilo_/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="_luckycoupon_sigilo_/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="_luckycoupon_sigilo_/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})

		setTimeout(function(){ 
            var msg = document.getElementsByClassName("messagem");
            while(msg.length > 0){
                msg[0].parentNode.removeChild(msg[0]);
            }
        }, 5000);
	</script>
<!--===============================================================================================-->
	<script src="_luckycoupon_sigilo_/js/main.js"></script>

</body>
</html>