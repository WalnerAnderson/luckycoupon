<?php
    session_start();
    if(!$_SESSION['usuarioId']){
        $_SESSION['error'] = "Faça seu Login!";
        header("Location: home");
    }
    include_once '_luckycoupon_sigilo_/_classes/Registros.php';
    include_once '_luckycoupon_sigilo_/modalMessage.php';

    modalMessage();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
   
    <title>Sorteio | Lucky Coupon</title>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="_luckycoupon_sigilo_/css/materialize.min.css">
    <link rel="stylesheet" href="_luckycoupon_sigilo_/css/dashboard.css">
    <link rel="stylesheet" href="_luckycoupon_sigilo_/css/sorteio.css">
</head>
<body>
    <nav>
        <div class="nav-wrapper">
            <a href="dashboard" class="brand-logo">Lucky Coupon</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="dashboard">REGISTRAR</a></li>
                <li><a href="sorteio">SORTEIO</a></li>
                <li><a href="ganhadores">RESULTADO</a></li>
                <li><a href="sair">SAIR</a></li>
            </ul>
        </div>
    </nav>
    <ul class="sidenav" id="mobile-demo">
        <li><a href="dashboard">REGISTRAR</a></li>
        <li><a href="sorteio">SORTEIO</a></li>
        <li><a href="ganhadores">RESULTADO</a></li>
        <li><a href="sair">SAIR</a></li>
    </ul>
    
    <div class="container area-sorteio">
        <div id="sorte">
            <h1>Lucky Coupon</h1>

            <?php if(empty($_SESSION['sortiado'])){ ?>
                <p class="aviso">Nenhum registro para sortiar</p>
                
            <?php }else{ ?>
            <h2 class="title-cupom">Cupom</h2>
            <p class="num"><?php echo $_SESSION['sortiado']->cupom;?></p>
            <p class="dados1"><span>Nome:</span> <?php echo $_SESSION['sortiado']->nome;?></p>
            <p class="dados"><span>Endereço:</span> <?php echo $_SESSION['sortiado']->endereco;?></p>
            <p class="dados"><span>RG:</span> <?php echo $_SESSION['sortiado']->rg;?></p>
            <p class="dados"><span>Telefone:</span> <?php echo $_SESSION['sortiado']->telefone; ?></p>
            <?php } ?>
            <div id="organization-btn">
                <div class="butao-resultado">
                    <a href="#modal1" onclick="trigger('<?php echo $_SESSION['sortiado']->id_registros; ?>', '<?php echo $_SESSION['sortiado']->cupom; ?>')" class="waves-effect btn-resultado waves-light btn modal-trigger">
                        Salvar
                    </a>
                </div>
                <div class="butao-resultado">
                    <a href="sorteio" class="waves-effect btn-resultado waves-light btn">
                        Cancelar
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>REGISTRE O PRÊMIO</h4>
            <form action="config/config_resultado" method="POST">
                <input id="id_registros" type="hidden" name="id_registros" class="validate id_registros" required>
                <input id="cupom" type="hidden" name="cupom" class="validate num_cupom" required>
                <div class="input-field">
                    <input id="premio" type="text" name="premio" class="validate premio" required>
                    <label for="premio">Informe qual o prêmio do ganhador</label>
                </div>
                <div class="button">
                    <button class="btn waves-effect waves-light" type="submit" name="salvar-resultado">Registrar</button>
                </div>    
            </form>
        </div>
    </div>
</body>
<script type="text/javascript" src="_luckycoupon_sigilo_/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="_luckycoupon_sigilo_/js/materialize.min.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var elemsModal = document.querySelectorAll('.modal');
        var instances = M.Sidenav.init(elems, elemsModal, options);
    });

    $(document).ready(function(){
        $('.sidenav').sidenav();
        $('.modal').modal();
    });

    function trigger(id_registros, cupom){
        $(".id_registros").val(id_registros);
        $(".num_cupom").val(cupom);
    }
</script>
</html>