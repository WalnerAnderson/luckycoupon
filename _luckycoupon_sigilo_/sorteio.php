<?php
    session_start();
    if(!$_SESSION['usuarioId']){
        $_SESSION['error'] = "Faça seu Login!";
        header("Location: home");
    }
    include_once '_luckycoupon_sigilo_/_classes/Registros.php';
    include_once '_luckycoupon_sigilo_/modalMessage.php';

    modalMessage();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
   
    <title>Sorteio | Lucky Coupon</title>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="_luckycoupon_sigilo_/css/materialize.min.css">
    <link rel="stylesheet" href="_luckycoupon_sigilo_/css/dashboard.css">
    <link rel="stylesheet" href="_luckycoupon_sigilo_/css/sorteio.css">
</head>
<body>
    <nav>
        <div class="nav-wrapper">
            <a href="dashboard" class="brand-logo">Lucky Coupon</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
            <li><a href="dashboard">REGISTRAR</a></li>
                <li><a href="sorteio">SORTEIO</a></li>
                <li><a href="ganhadores">RESULTADO</a></li>
                <li><a href="sair">SAIR</a></li>
            </ul>
        </div>
    </nav>
    <ul class="sidenav" id="mobile-demo">
        <li><a href="dashboard">REGISTRAR</a></li>
        <li><a href="sorteio">SORTEIO</a></li>
        <li><a href="ganhadores">RESULTADO</a></li>
        <li><a href="sair">SAIR</a></li>
    </ul>
    
    <div class="container area-sorteio">
        <div id="sorte">
            <h1>Lucky Coupon</h1>
            <p class="iniciar">Click agora e veja quem vai ser o sortudo da vez!</p>
            <div class="butao">
                <a href="config/config_registros?sorteio=true" class="waves-effect btn-large btn-sorte waves-light btn">
                    Sortear Agora
                </a>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript" src="_luckycoupon_sigilo_/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="_luckycoupon_sigilo_/js/materialize.min.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems, options);
    });

    $(document).ready(function(){
        $('.sidenav').sidenav();
    });
</script>
</html>