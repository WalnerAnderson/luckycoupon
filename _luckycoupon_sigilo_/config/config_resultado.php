<?php
    session_start();
    include_once '_luckycoupon_sigilo_/_classes/Resultado.php';
    
    if(isset($_POST['salvar-resultado'])){
        $id_registros = $_POST['id_registros'];
        $cupom        = $_POST['cupom'];
        $premio       = $_POST['premio'];

        $Resultado = new Resultado();
        $Resultado->setCupom($cupom);
        $Resultado->setFKRegistros($id_registros);
        $Resultado->setPremio($premio);

        if($Resultado->insert()){
            header("Location: ../sorteio");
        }
    }

    if(isset($_GET['excluir'])){
        $Resultado = new Resultado();
        if($Resultado->delete($_GET['excluir'])){
            $_SESSION['messageSucess'] = "Ganhador excluido com sucesso!";
            header("Location: ../ganhadores");

        }else{
            $_SESSION['messageError'] = "Erro ao excluir!";
            header("Location: ../ganhadores");
        }
    }