<?php
    session_start();
    include_once '_luckycoupon_sigilo_/_classes/Usuario.php';

    if((isset($_POST['email'])) && (isset($_POST['pass']))){
        $email = trim($_POST['email']);		
        $senha = trim($_POST['pass']);
        
        if(empty($email) || empty($senha)){
            $_SESSION['error'] = "Campos vazios!";
            echo "<script>javascript:history.back(-1)</script>";
            
        }else{
            $Usuario = new Usuario();
            $userResult = $Usuario->findUser($email, md5($senha));
    
            if(!empty($userResult)){
                $_SESSION['usuarioId']    = $userResult->id_usuario;
                $_SESSION['usuarioEmail'] = $userResult->email;
                $_SESSION['usuarioCargo'] = $userResult->senha;

                header("Location: ../dashboard");
            }else{
                $_SESSION['error'] = "Usuário ou senha Incorreto!";
                echo "<script>javascript:history.back(-1)</script>";
            }
        }  
    }else{
        $_SESSION['error'] = "Usuário ou senha não existem!";
        echo "<script>javascript:history.back(-1)</script>";
    }