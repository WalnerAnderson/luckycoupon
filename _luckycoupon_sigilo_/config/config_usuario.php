<?php
    session_start();
    include_once '_luckycoupon_sigilo_/_classes/Usuario.php';

    if(isset($_POST['insert'])){
        $email      = trim($_POST['email']);
        $senha      = trim($_POST['pass']);

        $Usuario = new Usuario();

        if($email == $Usuario->verifyUsuario($email)->email){
            $_SESSION['error'] = "Usuário existente!";
            echo "<script>javascript:history.back(-1)</script>";
        }else{
            $Usuario->setEmail($email);
            $Usuario->setSenha(md5($senha));
                  
            if($Usuario->insert()){
                $userResult = $Usuario->findUser($email, md5($senha));

                if(!empty($userResult)){
                    $_SESSION['usuarioId']    = $userResult->id_usuario;
                    $_SESSION['usuarioEmail'] = $userResult->email;
                    $_SESSION['usuarioCargo'] = $userResult->senha;
    
                    header("Location: ../dashboard");
                }
            }
        }
    }
                       