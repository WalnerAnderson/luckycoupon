<?php
    session_start();
    include_once '_luckycoupon_sigilo_/_classes/Registros.php';

    if(isset($_POST['registrar'])){
        $nome       = trim($_POST['tfNome']);
        $rg         = trim($_POST['tfRg']);
        $endereco   = trim($_POST['tfEndereco']);
        $telefone   = $_POST['tfTel'];
        $cupom      = trim($_POST['numCumpom']);

        if(empty($nome) || empty($rg) || empty($telefone) || empty($endereco) || empty($cupom)){
            $_SESSION['messageError'] = "Preencha todos os campos vazios!";
            header("Location: ../dashboard");

        }else{
            $Registros = new Registros();
            $existsRg = $Registros->verifyRg($rg);

            if($existsRg == false){
                $existsCupom = $Registros->verifyCupom($cupom, $_SESSION['usuarioId']);

                if($existsCupom == false){
                    $Registros->setNome($nome);
                    $Registros->setRg($rg);
                    $Registros->setTelefone($telefone);
                    $Registros->setEndereco($endereco);
                    $Registros->setCupom($cupom);
                    $Registros->setIdUsuario($_SESSION['usuarioId']);
    
                    if($Registros->insert()){
                        $_SESSION['messageSucess'] = "Cadastro realizado com sucesso!";
                        header("Location: ../dashboard");
    
                    }else{
                        $_SESSION['messageError'] = "Erro ao cadastrar!";
                        header("Location: ../dashboard");
                    }
                }else{
                    $_SESSION['messageError'] = "Cupom $cupom já resgistrado";
                    header("Location: ../dashboard");
                }
            }else{
                $_SESSION['messageError'] = "Cadastro existente!";
                header("Location: ../dashboard");
            }
        }

    }
    
    if(isset($_GET['excluir'])){
        $Registros = new Registros();
        if($Registros->delete($_GET['excluir'])){
            $_SESSION['messageSucess'] = "Registro Excluido com sucesso!";
            header("Location: ../dashboard");

        }else{
            $_SESSION['messageError'] = "Erro ao excluir!";
            header("Location: ../dashboard");
        }
    }
    
    if(isset($_POST['id_registros'])){
        $id_registros = trim($_POST['id_registros']);
        $nome         = trim($_POST['tfNome']);
        $rg           = trim($_POST['tfRg']);
        $endereco     = trim($_POST['tfEndereco']);
        $telefone     = $_POST['tfTel'];

        if(empty($nome) || empty($rg) || empty($telefone) || empty($endereco)){
            $_SESSION['messageError'] = "Preencha todos os campos vazios!";
            header("Location: ../dashboard");

        }else{
            $Registros = new Registros();
            $Registros->setNome($nome);
            $Registros->setRg($rg);
            $Registros->setTelefone($telefone);
            $Registros->setEndereco($endereco);

            if($Registros->update($id_registros)){
                $_SESSION['messageSucess'] = "Atualização realizada com sucesso!";
                header("Location: ../dashboard");

            }else{
                $_SESSION['messageError'] = "Erro ao atualizar!";
                header("Location: ../dashboard");
            }
        }
    }
    
    if(isset($_GET['sorteio'])){
        $Registros = new Registros();
        $_SESSION['sortiado'] = $Registros->sorteio($_SESSION['usuarioId']);

        header("Location: ../resultado");
    }