<?php
    session_start();
    if(!$_SESSION['usuarioId']){
        $_SESSION['error'] = "Faça seu Login!";
        header("Location: home");
    }
    include_once '_luckycoupon_sigilo_/_classes/Resultado.php';
    include_once '_luckycoupon_sigilo_/modalMessage.php';

    modalMessage();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
   
    <title>Dash board | Lucky Coupon</title>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="_luckycoupon_sigilo_/css/materialize.min.css">
    <link rel="stylesheet" href="_luckycoupon_sigilo_/css/dashboard.css">
</head>
<body>
    <nav>
        <div class="nav-wrapper">
            <a href="dashboard" class="brand-logo">Lucky Coupon</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="dashboard">REGISTRAR</a></li>
                <li><a href="sorteio">SORTEIO</a></li>
                <li><a href="ganhadores">RESULTADO</a></li>
                <li><a href="sair">SAIR</a></li>
            </ul>
        </div>
    </nav>

    <ul class="sidenav" id="mobile-demo">
        <li><a href="dashboard">REGISTRAR</a></li>
        <li><a href="sorteio">SORTEIO</a></li>
        <li><a href="ganhadores">RESULTADO</a></li>
        <li><a href="sair">SAIR</a></li>
    </ul>
    
    <div class="container ">
        <h1>Lista de Ganhadores</h1>

        <table class="highlight centered registro">
            <thead>
            <tr>
                <th>NOME</th>
                <th>RG</th>
                <th>ENDEREÇO</th>
                <th>TELEFONE</th>
                <th>CUPOM</th>
                <th>PRÊMIO</th>
                <th>EXCLUIR</th>
            </tr>
            </thead>
            <tbody>
            <?php
                $Resultado = new Resultado();
                foreach ($Resultado->findAllGanhadores($_SESSION['usuarioId']) as $key => $value) {
            ?>
            <tr>
                <td><?php echo $value->nome ?></td>
                <td><?php echo $value->rg ?></td>
                <td><?php echo $value->endereco ?></td>
                <td><?php echo $value->telefone ?></td>
                <td><?php echo $value->cupom ?></td>
                <td><?php echo $value->premio ?></td>    
                <td class="delete">
                    <a href="config/config_resultado?excluir=<?php echo $value->id_resultado?>" class="waves-effect btn-del waves-light btn">
                        <i class="material-icons">delete</i>
                    </a>
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <!-- Modal Structure -->
    <div id="modal2" class="modal">
        <div class="modal-content">
            <h4>INFORME OS DADOS DO CLIENTE</h4>
            <form action="config/config_registros" method="POST">
                <input id="id_registros" type="hidden" name="id_registros" class="validate id_registros" required>
                <div class="input-field">
                    <input id="nome" type="text" name="tfNome" class="validate nome" placeholder="NOME" required>
                    <label for="nome">NOME</label>
                </div>
                <div class="input-field">
                    <input id="rg" type="number" name="tfRg" class="validate rg" placeholder="RG" required>
                    <label for="rg">RG</label>
                </div>
                <div class="input-field">
                    <input id="enredeco" type="text" name="tfEndereco" class="validate endereco" placeholder="ENDEREÇO" required>
                    <label for="enredeco">Endereco</label>
                </div>
                <div class="input-field">
                    <input id="tel" type="text" name="tfTel" class="validate phone_with_ddd telefone" placeholder="TELEFONE" required>
                    <label for="tel">Telefone</label>
                </div>
                <div class="input-field">
                    <input id="cupom" type="number" name="numCumpom" class="validate cupom" placeholder="CUPOM" required readonly>
                    <label for="cupom">Numero do cupom</label>
                </div>
                <div class="button">
                    <button class="btn waves-effect waves-light" type="submit" name="update-registro">Atualizar</button>
                </div>    
            </form>
        </div>
    </div>
</body>
<script type="text/javascript" src="_luckycoupon_sigilo_/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="_luckycoupon_sigilo_/js/materialize.min.js"></script>
<script type="text/javascript" src="_luckycoupon_sigilo_/js/jquery.mask.min.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var elemsModal = document.querySelectorAll('.modal');
        var instances = M.Sidenav.init(elems, elemsModal, options);
    });

    $(document).ready(function(){
        $('.sidenav').sidenav();
        $('.phone_with_ddd').mask('(00) 00000-0000');
        $('.modal').modal();
        $('#modalMessage').modal("open");
    });

    function trigger(id_registros, nome, rg, telefone, endereco, cupom){
        $(".id_registros").val(id_registros);
        $(".nome").val(nome);
        $(".rg").val(rg);
        $(".telefone").val(telefone);
        $(".endereco").val(endereco);
        $(".cupom").val(cupom);
    }
</script>
</html>