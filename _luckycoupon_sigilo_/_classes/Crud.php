<?php
    include_once '_luckycoupon_sigilo_/_classes/db/Conect.php';

    abstract class Crud extends Conect{
        protected $table;
        protected $id;

        abstract public function insert();
        abstract public function update($id);

        public function findDesc(){
            $sql = "SELECT $this->id FROM $this->table ORDER BY $this->id Desc LIMIT 1";
            $stmt = Conect::prepare($sql);
            $stmt->execute();
            return $stmt->fetch();
        }

        public function find($id){
            $sql = "SELECT * FROM $this->table WHERE $this->id = :id";
            $stmt = Conect::prepare($sql);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch();
        }
        
        public function findAll($id){
            $sql = "SELECT * FROM $this->table WHERE id_usuario = $id ORDER BY $this->id DESC";
            $stmt = Conect::prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        public function delete($id){
            $sql = "DELETE FROM $this->table WHERE $this->id = :id";
            $stmt = Conect::prepare($sql);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->rowCount();
        }
    }