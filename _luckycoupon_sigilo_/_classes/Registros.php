<?php
    include_once '_luckycoupon_sigilo_/_classes/Crud.php';

    class Registros extends Crud{
        protected $table = 'registros';
        protected $id    = 'id_registros';
        private   $nome;
        private   $rg;
        private   $endereco;
        private   $telefone;
        private   $cupom;
        private   $id_usuario;
        
        public function getNome(){
            return $this->nome;
        }
        public function setNome($nome){
            $this->nome = $nome;
        }

        public function getRg(){
            return $this->rg;
        }
        public function setRg($rg){
            $this->rg = $rg;
        }

        public function getTelefone(){
            return $this->telefone;
        }
        public function setTelefone($telefone){
            $this->telefone = $telefone;
        }

        public function getEndereco(){
            return $this->endereco;
        }
        public function setEndereco($endereco){
            $this->endereco = $endereco;
        }

        public function getCupom(){
            return $this->cupom;
        }
        public function setCupom($cupom){
            $this->cupom = $cupom;
        }

        public function getIdUsuario(){
            return $this->id_usuario;
        }
        public function setIdUsuario($id_usuario){
            $this->id_usuario = $id_usuario;
        }

        public function insert(){
            $sql = "INSERT INTO $this->table (nome, rg, endereco, telefone, cupom, id_usuario) 
            VALUES(:nome, :rg, :endereco, :telefone, :cupom, :id_usuario)";
            $stmt = Conect::prepare($sql);
            $stmt->bindParam(':nome', $this->nome, PDO::PARAM_STR);
            $stmt->bindParam(':rg', $this->rg, PDO::PARAM_STR);
            $stmt->bindParam(':endereco', $this->endereco, PDO::PARAM_STR);
            $stmt->bindParam(':telefone', $this->telefone, PDO::PARAM_STR);
            $stmt->bindParam(':cupom', $this->cupom, PDO::PARAM_INT);
            $stmt->bindParam(':id_usuario', $this->id_usuario, PDO::PARAM_INT);
            return $stmt->execute();
        }

        public function verifyRg($rg){
            $sql = "SELECT rg FROM $this->table WHERE rg = :rg";
            $stmt = Conect::prepare($sql);
            $stmt->bindParam(':rg', $rg, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        }

        public function verifyCupom($cupom, $id){
            $sql = "SELECT rg FROM $this->table WHERE id_usuario = :id AND cupom = :cupom";
            $stmt = Conect::prepare($sql);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->bindParam(':cupom', $cupom, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch();
        }

        public function update($id){
            $sql = "UPDATE $this->table SET nome = :nome, rg = :rg, endereco = :endereco, telefone = :telefone WHERE $this->id = :id_registros";
            $stmt = Conect::prepare($sql);
            $stmt->bindParam(':nome', $this->nome, PDO::PARAM_STR);
            $stmt->bindParam(':rg', $this->rg, PDO::PARAM_STR);
            $stmt->bindParam(':endereco', $this->endereco, PDO::PARAM_STR);
            $stmt->bindParam(':telefone', $this->telefone, PDO::PARAM_STR);
            $stmt->bindParam(':id_registros', $id, PDO::PARAM_INT);
            return $stmt->execute();
        }

        public function buscar($search, $id){
            $sql = "SELECT * FROM $this->table WHERE id_usuario = $id AND nome LIKE '%$search%'";
            $stmt = Conect::prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        public function sorteio($id){
            $sql = "SELECT * FROM $this->table WHERE id_usuario = $id ORDER BY RAND() LIMIT 1";
            $stmt = Conect::prepare($sql);
            $stmt->execute();
            return $stmt->fetch();
        }
    }