<?php
    include_once '_luckycoupon_sigilo_/_classes/Crud.php';

    class Usuario extends Crud{
        protected $table = 'usuario';
        protected $id    = 'id_usuario';
        private   $email;
        private   $senha;

        public function getEmail(){
            return $this->email;
        }
        public function setEmail($email){
            $this->email = $email;
        }

        public function getSenha(){
            return $this->senha;
        }
        public function setSenha($senha){
            $this->senha = $senha;
        }

        public function findUser($email, $senha){
            $sql = "SELECT * FROM usuario WHERE usuario.email = :usuario && usuario.senha = :senha LIMIT 1";
            $stmt = Conect::prepare($sql);
            $stmt->bindParam(':usuario', $email, PDO::PARAM_STR);
            $stmt->bindParam(':senha',   $senha, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        }

        public function verifyUsuario($email){
            $sql = "SELECT email FROM $this->table WHERE email = :email";
            $stmt = Conect::prepare($sql);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        }

        public function insert(){
            $sql = "INSERT INTO $this->table (email, senha) VALUES(:email, :senha)";
            $stmt = Conect::prepare($sql);
            $stmt->bindParam(':email', $this->email, PDO::PARAM_STR);
            $stmt->bindParam(':senha', $this->senha, PDO::PARAM_STR);
            return $stmt->execute();
        }

        public function update($id){}
    }