<?php
    include_once '_luckycoupon_sigilo_/_classes/Crud.php';

    class Resultado extends Crud{
        protected $table = 'resultado';
        protected $id    = 'id_resultado';
        private   $cupom;
        private   $premio;
        private   $fk_Registros;
        
        public function getCupom(){
            return $this->cupom;
        }
        public function setCupom($cupom){
            $this->cupom = $cupom;
        }

        public function getPremio(){
            return $this->premio;
        }
        public function setPremio($premio){
            $this->premio = $premio;
        }

        public function getFKRegistros(){
            return $this->fk_Registros;
        }
        public function setFKRegistros($fk_Registros){
            $this->fk_Registros = $fk_Registros;
        }

        public function insert(){
            $sql = "INSERT INTO $this->table (cupom, id_registros, premio) VALUES(:cupom, :id_registros, :premio)";
            $stmt = Conect::prepare($sql);
            $stmt->bindParam(':cupom', $this->cupom, PDO::PARAM_STR);
            $stmt->bindParam(':id_registros', $this->fk_Registros, PDO::PARAM_INT);
            $stmt->bindParam(':premio', $this->premio, PDO::PARAM_STR);
            return $stmt->execute();
        }

        public function findAllGanhadores($id){
            $sql = "SELECT * FROM registros, resultado WHERE resultado.id_registros = registros.id_registros AND id_usuario = $id";
            $stmt = Conect::prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        public function update($id){}
    }