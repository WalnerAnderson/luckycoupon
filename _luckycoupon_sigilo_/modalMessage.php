<?php
    function modalMessage(){
        if(isset($_SESSION['messageError'])){ ?>
            <div id="modalMessage" class="modal renovar">
                <div class="modal-content">
                    <h4 class="modalError"><?php echo $_SESSION['messageError']; unset($_SESSION['messageError']); ?></h4>
                    <div>
                        <img src="_luckycoupon_sigilo_/icons/error.svg" alt="Icone de cadastro com erro, suaexperiencia.com">
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">SEGUIR</a>
                </div>
            </div>
        <?php }else if(isset($_SESSION['messageSucess'])){?>
            <div id="modalMessage" class="modal renovar">
                <div class="modal-content">
                    <h4><?php echo $_SESSION['messageSucess']; unset($_SESSION['messageSucess']); ?></h4>
                    <div>
                        <img src="_luckycoupon_sigilo_/icons/success.svg" alt="Icone de cadastro com sucesso, suaexperiencia.com">
                    </div>
                    <?php
                    if(isset($_SESSION['qtdClient'])){ ?>
                        <h6>Informações importantes:</h6>
                    
                        <?php if($_SESSION['qtdClient'] > 0){ ?>  
                            <p class="valido"><?php echo $_SESSION['qtdClient']; unset($_SESSION['qtdClient']); ?> Clientes registrados</p>    
                        <?php }}
                        if(isset($_SESSION['qtdEmailInvalido'])){ 
                            if($_SESSION['qtdEmailInvalido'] > 0){ ?>
                            <p class="inValido"><?php echo $_SESSION['qtdEmailInvalido']; unset($_SESSION['qtdEmailInvalido']); ?> Clientes invalidos</p>
                    <?php }} ?> 
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">SEGUIR</a>
                </div>
            </div>
        <?php } 
    } 